---
id: tutorial-pbt
title: tutorial-PBT
sidebar_label: Getting Started with Pro BTC Trader
---

# Getting Started with Pro BTC Trader

Pro BTC Trader is your daily email, sent at 8am EST (USA time), with price trends and predictions on where Bitcoin is headed to help you identify potential trades. Please contact us at customerservice@cryptobriefing.com if you do not receive these daily emails.

Otherwise, to make sure you always receive our emails, reply back to the "getting started" email you received after you signed up with a one, or more, word message (e.g., "HODL"). This will help prevent our emails from being blocked or marked as spam by your email server.

# What you get and how to use Pro BTC Trader

## Pro BTC Trader

Each email contains an overview of where bitcoin is at and is likely going based on our lead Bitcoin analyst, Nathan Batchelor's, **FOSSP framework**.

- **Fundamental analysis** - Reviewing the impact of macro-economic trends like interest rates and major industry news on Bitcoin.
- **On-chain analysis** - Analyzing current "whale" activity on-chain to detect market changing transfers of coins.
- **Sentiment analysis** - Analyzing social media to show which way the herd is leaning on channels such as Twitter, Reddit, and Telegram.
- **Price action** - Analyzing price fluctuations to detect unnatural price flows, especially during price pumps.
- **Patterns** - Using technical analysis to detect upcoming reversals in the direction of prices.

## LIVE Trading War Room

<iframe class="player" src="https://player.vimeo.com/video/558246134?dnt=1&amp;app_id=122963" frameborder="0" allowfullscreen="allowfullscreen" data-mce-fragment="1"></iframe>

Each week, you will also have access to the LIVE Trading War Room. This is a 30-minute live webinar on Wednesdays, 1130am EST (USA time) hosted by Nathan where he will share

- The fundamental factors influencing the crypto-currency markets
- An up-to-date look at BTC/USD on the charts through various time frames and indicators
- New developing trends and a walk through of chart analysis
- Forward looking predictions on where Bitcoin is headed

In these weekly webinars, Nathan goes through popular chart analysis strategies and other educational topics to help you better understand what’s happening in the market. You'll also have a chance to ask questions and watch Nathan chart other interesting or major coins like ETH, DOT and ADA.

You can watch recordings of **[previous webinars here](https://simetri.cryptobriefing.com/webinars/)**.

To watch and participate live, click the RSVP link sent by email on Mondays or 10 minutes prior to starting.

## Sample Pro BTC Trader Email

Whale Data Pro BTC Trader: Daily Commentary and Trade Alert

![Docs Version Dropdown](/img/1.jpg)

## Useful links

Learn about risk-reward ratios, market phases, and how much to risk per trade **[here](https://simetri.cryptobriefing.com/constructing-a-crypto-trading-portfolio/)**.

Learn more about margin trading and short selling **[here](https://simetri.cryptobriefing.com/margin-trading)** and **[here](https://simetri.cryptobriefing.com/how-to-short-on-kraken)**.

Consider also reading our **[Crypto Investment Strategy: Part Three - The Active Trader](https://simetri.cryptobriefing.com/crypto-investing-strategies-part-three-the-active-trader)**.

Your profitability will come down to proper position sizing. Please read our article on **[Constructing a Trading Portfolio and Position Sizing](https://simetri.cryptobriefing.com/constructing-a-crypto-trading-portfolio/)** before attempting your first trade.

Further, it’s not a good idea to start trading with real money if you haven't traded crypto before. Consider starting with **[paper trades](https://www.investopedia.com/articles/active-trading/072915/pros-and-cons-paper-trading.asp)** for several weeks before trading with real money.

## SIMETRI News

Head over to our blog to find all the latest announcements, trade alerts, market insights, trading strategies, and various other educational content for those new to crypto investing.

You can find our blog **[here](https://simetri.cryptobriefing.com/news)**.

## Questions and Support

If you ever have any questions about your account or have login issues, just email us at **customerservice@cryptobriefing.com** or find the “Help” button on the lower-right corner of **[https://simetri.cryptobriefing.com/](https://simetri.cryptobriefing.com/)** and submit a support ticket.

## Disclaimer

And as always, for all our reports, it is important to remember that we are not registered financial advisers with the SEC or any other organization or governmental body, and what we provide is not a personalized investment advice. Nor are our reports and flashes an offer to purchase or sell any particular token related to a project. These are merely the best opinions of our research team based upon our various research and diligence. But this is no substitute for personalized advice from an investment professional. Always DYOR (do your own research). Investments in tokens and crypto currencies are highly speculative, and prices of tokens can be quite volatile, so doing so may not be suitable for you.

Please see our **[full disclaimer and terms of use here](https://simetri.cryptobriefing.com/privacy-policy/)**.
