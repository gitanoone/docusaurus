---
id: tutorial-simetri
title: tutorial-SIMETRI
sidebar_label: Getting Started with SIMETRI
---

# Getting Started with SIMETRI

Here we explain what you get as part of your SIMETRI memberships, when you get it, and how you can use it to advance your crypto knowledge and make smarter investments.

If you subscribe to SIMETRI, you will receive an email notification from us every time we publish a new report. **To make sure our emails always arrive safely in your primary inbox,** simply locate the "getting started" email you received from us when you joined and reply to it with anything in the message and hit send. (This will tell your email provider that our emails are important you).

# What you get and how to use SIMETRI

## Pick-of-the-Month Reports

Our **Pick of the Month** report is released once a month on the **second Tuesday of every month at 1pm Eastern Time.** They are crypto prospects that we believe possess strong fundamentals but also have the potential to generate explosive gains in the **short-term** (1-3 month horizon).

We believe these are worth investing in immediately upon report release.

We will provide a **“Buy up to Price”** in the “Quick Facts” section (first page) of the report. This price, in our opinion, is the highest price per coin that would still make the project an attractive investment.

Depending on market conditions, if we feel it may be time to take profits we will send you a sell alert. You can find recent trade alerts **[here](https://simetri.cryptobriefing.com/news/trade-alerts)**

These reports are good for both long-term holders as well as short term traders.

Pick of the Month reports fall in line with **[Crypto Investment Strategy: Part Two - Playing Venture Capitalist](https://simetri.cryptobriefing.com/crypto-investing-strategies-part-two-playing-venture-capitalist/)**.

Learn more about how to construct a **[crypto portfolio and properly size your position](https://simetri.cryptobriefing.com/position-sizing-and-how-to-construct-a-crypto-portfolio/)**

Pick of the Month reports can be found **[here](https://simetri.cryptobriefing.com/reports/)**. You will also receive an email notification once it is published.

We reserve the right to not release a pick if market conditions are not right for the month.

## Full Digital Asset Reports

These are in-depth analysis reports that assess the **long term** (one-year horizon) prospects of top coins and their respective investment rating.

We consider anything that gets a **B or higher an investable project for the long term.** These are coins that we believe are worth hanging onto for the long run.

In the “Quick Facts” section of the report (first page) you will find the **“Buy up to price”.** This price, in our opinion, is the highest price that would still make the coin an attractive investment.

Update reports and grade changes are provided when necessary - e.g. A major change to fundamentals like market opportunity, technology or ecosystem.

These reports are great for less active crypto traders looking for strong projects to hold onto.

Digital Asset Reports fall in line with **[Crypto Investment Strategy: Part One - Buy and Hold “Blue Chips”](https://simetri.cryptobriefing.com/crypto-investing-strategies-part-one-buy-and-hold-blue-chips/)**

Our library of Digital Asset Reports can be found **[here](https://simetri.cryptobriefing.com/reports/)**.

We’ve put together a 2-part educational series on how we evaluate crypto projects here **[(Part I)](https://vimeo.com/344346191)** and here **[(Part II)](https://vimeo.com/344581332)**.

To learn more about our evaluation framework and to understand what our grades mean, head to our **[methodology and grading section](https://simetri.cryptobriefing.com/methodology/)**.

## Coins on the Move Dashboard

**Coins on the Move** is a short-term signals dashboard that combines the projects’ fundamental developments along with short term factors and catalysts.

**[Coins on the Move](https://simetri.cryptobriefing.com/coins-on-the-move/)** provides you with:

- An easy way to get quick and actionable short-term investment signals.
- Entry, target and stop-loss prices.
- Insights on near-term projections and the reasoning for the expected move.
- The short term cards on Coins on the Move are also in line with **[Crypto Investment Strategy: Part Three - The Active Trader](https://simetri.cryptobriefing.com/crypto-investing-strategies-part-three-the-active-trader/)**.

You can find the Coins-on-the-Move Dashboard **[here](https://simetri.cryptobriefing.com/coins-on-the-move/)**.

Learn more about how Coins on the Move works **[here](https://simetri.cryptobriefing.com/cotm-guide/)**.

## SIMETRI News

Head over to our blog to find all the latest announcements, trade alerts, market insights, trading strategies, and various other educational content for those new to crypto investing.

You can find our blog **[here](https://simetri.cryptobriefing.com/news/)**.

## Questions and Support

If you ever have any questions about your account or have login issues, just email us at **customerservice@cryptobriefing.com** or find the “Help” button on the lower-right corner of **[https://simetri.cryptobriefing.com/](https://simetri.cryptobriefing.com/)** and submit a support ticket.

## Disclaimer

And as always, for all our reports, it is important to remember that we are not registered financial advisers with the SEC or any other organization or governmental body, and what we provide is not a personalized investment advice. Nor are our reports and flashes an offer to purchase or sell any particular token related to a project. These are merely the best opinions of our research team based upon our various research and diligence. But this is no substitute for personalized advice from an investment professional. Always DYOR (do your own research). Investments in tokens and crypto currencies are highly speculative, and prices of tokens can be quite volatile, so doing so may not be suitable for you.

Please see our **[full disclaimer and terms of use here](https://simetri.cryptobriefing.com/privacy-policy/)**.
